<?php 

class RouteException extends Exception{
	private $mLastRoute = null;
	
	public function getRoute(){
		return $this->mLastRoute;
	}
	
	public function setRoute($aValue) {
		$this->mLastRoute = $aValue;
	}
	

}

