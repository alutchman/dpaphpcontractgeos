<?php
class AuthException extends Exception {
	private $mControllerName ='';
	private $mActionName  ='';
	
	public function getAction(){
		return $this->mActionName;
	}
	
	
	public function setAction($aAction){
		$this->mActionName = $aAction;
	}
	
	
	public function getController(){
		return $this->mControllerName;
	}
	
	
	public function setController($aControllerName){
		$this->mControllerName = $aControllerName;
	}
	
}