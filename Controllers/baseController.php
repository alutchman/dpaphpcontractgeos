<?php

abstract class BaseController {
	protected $mAdmin;
	protected $mAction     = null;
	protected $mParams     = array();
	protected $mBaseAction = '';
	protected $mTemplateFile ='index.html';
	protected $mTemplatePath;
	protected $mException = null;
	protected $mModelList = array();
	protected $isMobile = false;
	
	
	protected $homeShow = 'display:block;';

	public final function __construct(Administrator &$aAdmin, $aParams, Exception $aException= null){
		$this->mAdmin = &$aAdmin;
		$this->mParams['request'] = $aParams;

		$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
		
		$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
		$this->mParams['tomorrow'] = date('d F Y', $tomorrow);
		$this->mParams['protocol'] = $protocol;		
		$this->mParams['serverPrefix'] = BASEPATH;
		$this->mParams['theServer'] = $_SERVER['SERVER_NAME'];
		$this->mParams['applPrefix'] = $protocol.'://'.$_SERVER['SERVER_NAME'].BASEPATH;
		
		
		$this->mBaseAction = $this->mAdmin->getAction();
		$this->mAction= sprintf('action_%s', $this->mBaseAction);
		$this->mException = $aException;
		
		$this->isMobile  = stristr($_SERVER['HTTP_USER_AGENT'], "Mobile" );
		$this->mParams['mobile'] = $this->isMobile ? 'YES' : 'NO';
		
		if ($this->isMobile) {
			$this->mTemplateFile = sprintf('%s.mobile.html', $this->mBaseAction);
		} else {
			$this->mTemplateFile = sprintf('%s.html', $this->mBaseAction);
		}
		
		
		
		$this->mParams['title'] = "Welkom Bij DPA";
	}
	
	
	public function hideHowShow(){
		$this->homeShow = 'display:none;';
	}
	
	
	public function setTitle($aTtile){
		$this->mParams['title'] = !is_null($aTtile)  ?  $aTtile : '';
	}


	public final function executeAction(){
		$lController = get_called_class();
		$lControllerStripped = substr($lController,0, -10);
		$this->mTemplatePath = $lControllerStripped;//.DIRECTORY_SEPARATOR.$this->mBaseAction;
		$this->{$this->mAction}();
	}
	

	protected function showTemplate($aBuffering = true){
		$lLoadedTemplate = new TemplateParser($this->mTemplatePath, $this->mTemplateFile, $this->mParams);

		$lLoadedTemplate->setData('homeShow',$this->homeShow);
		
		
		foreach($this->mModelList as &$lModel) {
			$lModel->show($lLoadedTemplate);
		}
		if (!is_null($this->mException)){
			$lLoadedTemplate->setData('ExceptionClass', get_class($this->mException));
			$lLoadedTemplate->setData('ExceptionMessage', $this->mException->getMessage());
			$lLoadedTemplate->setData('ExceptionTrace'  , $this->mException->getTrace());
		}
		$lLoadedTemplate->show();
	}
	
	protected function showLayout($layout, $aBuffering = true){
		$lLoadedTemplate = new TemplateParser($this->mTemplatePath, $this->mTemplateFile, $this->mParams);

		$lLoadedTemplate->setData('homeShow',$this->homeShow);
		$lLoadedTemplate->setLayout($layout);
	
		foreach($this->mModelList as &$lModel) {
			$lModel->show($lLoadedTemplate);
		}
		if (!is_null($this->mException)){
			$lLoadedTemplate->setData('ExceptionClass', get_class($this->mException));
			$lLoadedTemplate->setData('ExceptionMessage', $this->mException->getMessage());
			$lLoadedTemplate->setData('ExceptionTrace'  , $this->mException->getTrace());
		}
		$lLoadedTemplate->show();
	}
	
	protected function fetchLayoutPdf($layout, $otherTemplate = null){
		if ($otherTemplate != null) {			
			$lTemplateFile = $otherTemplate;
		} else {
			$lTemplateFile = sprintf('%s.html', $this->mBaseAction);
		}
		
		$lLoadedTemplate = new PdfHtmlTemplHandler($this->mTemplatePath, $lTemplateFile, $this->mParams);
				

		$lLoadedTemplate->setData('homeShow',$this->homeShow);
		$lLoadedTemplate->setLayout($layout);
		$lLoadedTemplate->updateParams( $this->mParams);
	
		foreach($this->mModelList as &$lModel) {
			$lModel->show($lLoadedTemplate);
		}
		if (!is_null($this->mException)){
			$lLoadedTemplate->setData('ExceptionClass', get_class($this->mException));
			$lLoadedTemplate->setData('ExceptionMessage', $this->mException->getMessage());
			$lLoadedTemplate->setData('ExceptionTrace'  , $this->mException->getTrace());
		}
		return $lLoadedTemplate->show();
	}
	
	
	protected function fetchPdf($otherTemplate = null){
		if ($otherTemplate != null) {
			$lTemplateFile = $otherTemplate;
		} else {
			$lTemplateFile = sprintf('%s.html', $this->mBaseAction);
		}
	
		$lLoadedTemplate = new PdfHtmlTemplHandler($this->mTemplatePath, $lTemplateFile, $this->mParams);	
	
		$lLoadedTemplate->setData('homeShow',$this->homeShow);
		$lLoadedTemplate->updateParams( $this->mParams);
	
		foreach($this->mModelList as &$lModel) {
			$lModel->show($lLoadedTemplate);
		}
		if (!is_null($this->mException)){
			$lLoadedTemplate->setData('ExceptionClass', get_class($this->mException));
			$lLoadedTemplate->setData('ExceptionMessage', $this->mException->getMessage());
			$lLoadedTemplate->setData('ExceptionTrace'  , $this->mException->getTrace());
		}
		return $lLoadedTemplate->show();
	}
	
	
	


	protected function redirect($altPath){
		$this->mAdmin->redirect($altPath);
	}


	protected final function execute(BaseModel $lNextModel) {
		$this->mModelList[] = $lNextModel;
		$lNextModel->handle();
	}


	public function getParam($aKey){
		return array_key_exists($aKey, $this->mParams)  ?  $this->mParams[$aKey] : '';
	}


	public function __call($name, Array $arguments) {
		$lException = new ActionException(sprintf('An error ocurred in the application: <span style="color:red;">protected function %s()</span> is not defined in the `%s` class.', $name, $this->mAdmin->getControllerClass()));
		$lException->setController($this->mAdmin->getControllerClass());
		$lException->setAction($name);
		throw $lException;
	}


	public final  function executeAltAction($aAction){
		$this->mBaseAction = $aAction;
		$this->mAction= sprintf('action_%s', $this->mBaseAction);		

		$this->mTemplateFile = sprintf('%s.html', $this->mBaseAction);
		
		
		$this->executeAction();
	}

}