<?php
	
class ErrorController extends BaseController {

	
	protected function action_noAuth(){
		if (!is_null($this->mException)){
			
		  $this->mParams['controller'] = $this->mException->getController();
		  $this->mParams['action'] = $this->mException->getAction();
		}
		$this->showTemplate();
	}


	protected function action_general(){
		$this->showTemplate('general.html');
	}
	

}