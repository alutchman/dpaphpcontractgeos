<?php

class MainController extends BaseController{


	protected function action_index(){	
		$this->setTitle('Kies de voorwaarden voor uw contract');
		$this->execute(new ContractStartModel($this->mParams));
		$this->execute(new DpaXmlModel($this->mParams));
		$this->showLayout($this->isMobile ? LAYOUT_MOBILE : LAYOUT);
		
	}
	
	
	protected function action_download(){			
		if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
			throw new ModelHandlerException('Only Post Method is supported for  the action start.');
			exit();
		}
	
		if (!isset($_SESSION['gekozen'])) {
			throw new ModelHandlerException('Invalid Request');
			exit();
		}
		
		$lTitle = 'Uw concept contract bij DPA';
		$this->setTitle($lTitle);
		$this->execute(new rateWageTableModel($this->mParams));
	
				
		$contentContract = $this->fetchPdf('contractOnbTijd.html');
		$lContactInfo = new ContactInfo(ADMIN_MAIL, ADMIN_MAIL_ACCESS,$this->mParams['request'] );
		
		$lCreateDocs = array();
		$lCreateDocs['contract.pdf'] = $contentContract;		

		$mailModel = new PhpMailModel($lContactInfo);
		$lDpaDocs =array();
		//$lDpaDocs[] = array('type'=>'file', 'location'=>DPA_DOCS_DIR.DIRECTORY_SEPARATOR.'overdpa.pdf');
		$dpaModelPdf = new PdfModel();
		$dpaModelPdf->sendHtml($contentContract, $lTitle);
		
		$lResult = $dpaModelPdf->Output('contract.pdf', 'S');		
		$contentEmail = $this->fetchLayoutPdf(DOWNLOAD_HTML);
		$lDpaDocs[] = array('type'=>'text', 'data'=>$lResult, 'file'=>'contract.pdf' );		
		$lResultEmail = $mailModel->send('Uw concept contract bij DPA', ($contentEmail), $lDpaDocs);	

		/*
		 * Added to determine if mail is allowed on system
		 * Oké, je kunt via UNC \\juliet\geoscalculatortest bij de site http://geoscalculatortest.dpa.nl
		 */
		if ($lResultEmail === true) {
			echo 'Een concept contract is naar u gemailed. DPA zal binnenkort contact met u opnemen: ';
		} else {
			echo $lResultEm;
		}
	}

	

	protected function action_ajaxResetSession(){
		unset($_SESSION['xmlOptions']);
	}
	
	
	protected function action_ajaxLoadInfo(){
		$this->execute(new rateWageTableModel($this->mParams));
		$this->showTemplate();

	}


	

}