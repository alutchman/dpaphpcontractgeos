<?php


class ContactInfo {
	public $username;
	public $access;	
	public $mailto;
	public $mailToUser;
	public $resourceName;
	public $resourceEmail;
	
	public function __construct($username, $access, $lRequest) {
		$lMailto     = isset($lRequest['email']) && strlen(trim($lRequest['email'])) > 0 ? trim($lRequest['email']) : null ;
		$lMailtoUser = trim($lRequest['voornaam']).' '.trim($lRequest['achternaam']);
		
		$this->mailToUser = trim($lMailtoUser);
		$this->mailto = $lMailto;
		
		$this->username = $username;
		$this->access = $access;
		
		$this->resourceEmail = DPA_RESOURCE_EMAIL;
		$this->resourceName = DPA_RESOURCE_NAME;
		
	}
	
	
	
}