<?php 

  require_once(PDF_HTML_INCLUDE.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php');
  

  require_once(PDF_HTML_INCLUDE.DIRECTORY_SEPARATOR.'_class'.DIRECTORY_SEPARATOR.'locale.class.php');
  require_once(PDF_HTML_INCLUDE.DIRECTORY_SEPARATOR.'_class'.DIRECTORY_SEPARATOR.'exception.class.php');
  require_once(PDF_HTML_INCLUDE.DIRECTORY_SEPARATOR.'_class'.DIRECTORY_SEPARATOR.'myPdf.class.php');
  require_once(PDF_HTML_INCLUDE.DIRECTORY_SEPARATOR.'_class'.DIRECTORY_SEPARATOR.'parsingCss.class.php');
  require_once(PDF_HTML_INCLUDE.DIRECTORY_SEPARATOR.'_class'.DIRECTORY_SEPARATOR.'parsingHtml.class.php');

  require_once(PDF_HTML_INCLUDE.'/html2pdf.class.php');


class HtmlPdfModel {
	private $pdf;
	

	public function __construct() {
		$this->pdf  = new HTML2PDF('P','A4','nl');
		
		
	}
	
	
	public function sendHtml($content, $filename){
		$this->pdf->WriteHTML($content);
		$this->pdf->Output($filename, 'D');
	}
	
	
	
}