<?php 

require_once(PDF_PAD_INCLUDE.'/tcpdf_autoconfig.php');
require_once(PDF_PAD_INCLUDE.'/tcpdf_import.php');
require_once(PDF_PAD_INCLUDE.'/tcpdf_parser.php');
require_once(PDF_PAD_INCLUDE.'/tcpdf.php');


class PdfModel {
	private $pdf;
	private $lastBuffer = '';
	
		
	public function __construct() {
		$this->pdf = new PdfWithLayout();
		$this->pdf->setLogo(DPA_LOGO);
	}
	
	
	public function sendHtml($content, $lTitle){
		$this->pdf->sendHtml($content, $lTitle);
	}
	
	public function Output($filename='doc.pdf', $dest='D'){
		$this->lastBuffer = $this->pdf ->Output($filename, $dest);
		return $this->lastBuffer;
	}
	
	
	public  function downlaod($asFile){
		header("Content-Type: application/octet-stream");
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"$asFile\"");
		

		header('Content-Length: '.strlen($this->lastBuffer));
		
		echo $this->lastBuffer;
		exit;
		
	}
	

	
	
}