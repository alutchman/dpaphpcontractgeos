<?php


require __DIR__.'/PHPMailer/PHPMailerAutoload.php';



class phpMailModel {
	private $mail;
	private $contactInfo;
	private $uploadedFiles;
	
	public function __construct(ContactInfo $aContactInfo) {
		$this->contactInfo = $aContactInfo;
		
		//Create a new PHPMailer instance
		$this->mail = new PHPMailer;
		// Set PHPMailer to use the sendmail transport
		$this->mail->isSMTP();
		//Set who the message is to be sent from
		$this->mail->setFrom($this->contactInfo->resourceEmail, $this->contactInfo->resourceName);
		
		$this->mail->SMTPDebug = 0;
		//Ask for HTML-friendly debug output
		$this->mail->Debugoutput = 'html';
		//Set the hostname of the mail server
		$this->mail->Host = "smtp.office365.com";
		//Set the SMTP port number - likely to be 25, 465 or 587
		$this->mail->Port = 587;
		//Whether to use SMTP authentication
		$this->mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$this->mail->Username = $this->contactInfo->username;
		//Password to use for SMTP authentication
		$this->mail->Password = $this->contactInfo->access;
	
	}
	
	public function send($aSubject , $aBody, $pdfAttachement = array()) {
		
		//Set an alternative reply-to address
		$this->mail->addReplyTo('info@dpa.nl', 'Dpa Information Site');
		//Set who the message is to be sent to
		
    	$this->mail->addAddress($this->contactInfo->mailto, $this->contactInfo->mailToUser);
		$this->mail->addCC($this->contactInfo->resourceEmail, $this->contactInfo->resourceName);
		
		//Set the subject line
		$this->mail->Subject = $aSubject;
		
		$this->mail->Body = $this->mail->msgHTML($aBody, PROJECTPATH);
		$this->mail->AltBody = 'This is a plain-text message body';
		
		//$this->mail->addStringAttachment('/home/amrit/Downlaods/AmritLutchmanDPACVNL20160815.docx');
		$lUploaded = array();
		foreach ($_FILES as $lFieldName=>$lFileData) {
			if(!isset($_FILES[$lFieldName]["error"]) || $_FILES[$lFieldName]["error"] <= 0){
				$lFilename = $_FILES[$lFieldName]['tmp_name'];
				$lSize = $_FILES[$lFieldName]["size"];
				if ($lSize  <= 1048577) {
					$lFileName = basename( $_FILES[$lFieldName]['name']);					
					$lFileType = $_FILES[$lFieldName]['type'];			
					$this->mail->addAttachment($_FILES[$lFieldName]['tmp_name'], $lFileName, 'base64', $lFileType);
				}
				$lUploaded[] = $_FILES[$lFieldName]['tmp_name'];
			}
		}	
		
		//=== hard path 
		foreach ($pdfAttachement as $atEntry) {
			if ($atEntry['type'] == 'file') {
				$this->mail->addAttachment($atEntry['location']);
			} elseif ($atEntry['type'] == 'text') {
				$this->mail->addStringAttachment($atEntry['data'] , $atEntry['file']);
			}
			
		}

		
		$lMailSend = $this->mail->send();
		
		//=== clean up uploaded files ...
		foreach ($lUploaded as $lFile2Send) {
			unlink($lFile2Send);
		}
		
		//send the message, check for errors
		if (!$lMailSend) {
			return "Mailer Error: " . $this->mail->ErrorInfo;
		} else {
			return true;
		}
	
	}
}