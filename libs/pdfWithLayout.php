<?php


require_once(PDF_PAD_INCLUDE.'/tcpdf_autoconfig.php');
require_once(PDF_PAD_INCLUDE.'/tcpdf_import.php');
require_once(PDF_PAD_INCLUDE.'/tcpdf_parser.php');
require_once(PDF_PAD_INCLUDE.'/tcpdf.php');


class PdfWithLayout extends TCPDF {
	private $logo = null;	
	const StartY = 3;	
	const HeaderMargin = 15;
	
	public function __construct() {
		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->SetMargins(PDF_MARGIN_LEFT, self::HeaderMargin, PDF_MARGIN_RIGHT);
		$this->SetHeaderMargin(self::HeaderMargin);
		$this->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		$this->setPrintHeader(true);
		$this->setPrintFooter(true);
		$this->SetFont('helvetica', '', 11);
		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM+2);
	}
	
	
	
	public function setLogo($logo){
		$this->logo = $logo;
	}
	
	public function Header() {
		$this->SetFont('helvetica', 'B', 14);
		$html = '<table style="border-bottom: 1px solid black;" >';
		$html .= '<tr><td> <img src="'.$this->logo.'" height="1cm"/> </td> <td>'.$this->title .'  </td>  </tr>';
		$html .= '</table>';
		$this->writeHTMLCell(0, 1, $this->lMargin, self::StartY, $html);
		
	}
	
	// Page footer
	public function Footer() {
		$this->SetY(-1*PDF_MARGIN_BOTTOM);
		// Set font
		$this->SetFont('helvetica', 'I', 10);
		// Page number

		$html = '';
		$html.= '<table cellpadding="1px"  cellspacing="0" style="border-top: 1px solid black;" >';
		$html .= '<tr>
					<td style="">Paraaf medewerker<br /> </td>	
				   <td>
						&nbsp;
				    </td>
					<td style="text-align:right;"> Paraaf DPA  <br /> </td> 
				</tr>';
		$html .= '<tr>
					<td>................................... </td>				
				    <td style="text-align:center;">
						Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'
				    </td>
					<td style="text-align:right;"> .................................... </td>
				</tr>';
		$html .= '</table>';		
		$this->writeHTML($html);
	}
	
	public function sendHtml($content, $lTitle){
		$this->SetTitle($lTitle);
		$this->AddPage();
		$this->SetY($this->tMargin+self::StartY);
		$this->writeHTML($content);
		
		
	}
}