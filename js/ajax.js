jQuery(document).ready(function() {
	// pre-submit callback
	/**
	 * Set global AJAX parameters
	 */
	jQuery.ajaxSetup({
	  async : true,
	  error : function(request, settings) {
		  /* alert('Error loading data on url: ' + settings.url); */
	  }

	});

	/**
	 * AJAX status messages
	 */
	jQuery(document).ajaxStart(function() {
		/** block UI */

	});

	jQuery(document).ajaxStop(function() {
		/** block UI */

	});

	jQuery(document).ajaxComplete(function() {

	});

	document.oncontextmenu = function() {
		// return false;
	};

	$('table.data tr.rowData').removeClass('even');
	$('table.data tr.rowData:even').addClass('even');
});