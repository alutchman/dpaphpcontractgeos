<?php


class rateWageTableModel extends AbstractModel{
	
	private $xmlOptions;

	
	private function updateSelectedOptions(&$lRequest) {	
		$selectedWage = intval($lRequest['wage']);
		$lWages2Rates = unserialize($_SESSION['Wages2Rates']);
	
		foreach ($lWages2Rates as $lKey=>&$lValue) {
			$lSectedAttrib = intval($lValue['wage']) == $selectedWage ? 'selected="SELECTED"'  : ''	;
			$lValue['selected'] = $lSectedAttrib;
			$lValue['wage'] = intval($lValue['wage']);
			$lValue['rate'] = intval($lValue['rate']);
		}
			
		$lRate = array_key_exists(intval($this->mParams['request']['wage']), $lWages2Rates) ? floatval($lWages2Rates[intval($this->mParams['request']['wage'])]['rate']) : 0;
		
		$lGekozenOpties = array();

		$lGekozenOpties[] = array('value'=> '&euro; '.$selectedWage, 'display'=>'Gewenste Loon');

		foreach ($this->xmlOptions->option as $option) {
			$lAllowEmptie = isset($option->attributes()->allowEmpty) ? strval($option->attributes()->allowEmpty) : 'true';
			$lSelectBoxName = strval($option->attributes()->name);
			if (array_key_exists($lSelectBoxName, $lRequest)) {
				$lCost = $lRequest[$lSelectBoxName];				
				$lRate += floatval($lCost);
				$lTitle = strval($option->attributes()->title);
				
				foreach ($option->item as $listItem) {
					$lCostOption =  strval($listItem->attributes()->value );
					if ($lCostOption == $lCost) {
						$lSelectedDisplay = strval($listItem->attributes()->display );
						$lGekozenOpties[] = array('value'=> $lSelectedDisplay, 'display'=>$lTitle);
					}
				}
			}
		}
		$this->mParams['Gekozen'] = $lGekozenOpties;
		
	
		/*
		$lStartRate = intval(10*round(floatval($lRate/10)))-5;
		if ($lStartRate < 55) $lStartRate = 55;
	
		$lRate2Show = array();
		for ($i=$lStartRate; $i<=120; $i+=5) {
			$lRate2Show[] = array('tarief'=>$i, 'wage'=>$selectedWage);
			if (count($lRate2Show) == 8) break;
		}
		*/
		$lStartRate = intval($lRequest['extTarief'])-5;
		if ($lStartRate + 20 >= 120) {
			$lStartRate = 120-20;
		}
		$lRate2Show = array();
		for ($i=$lStartRate; $i<=120; $i+=5) {
			$lRate2Show[] = array('tarief'=>$i, 'wage'=>$selectedWage);
			if (count($lRate2Show) == 4) break;
		}
				
		$this->calcualteWages($selectedWage, $lRate, $lRate2Show);
		
		
		$lRequest['rates2show'] = $lRate2Show;
		$lRequest['rate'] = $lRate;

		$_SESSION['gekozen'] = serialize($lRequest);
		$_SESSION['Wages2Rates'] = serialize($lWages2Rates);
	}
	
	
	private function calcualteWages($selectedWage, $lRate, &$lRate2Show){
		foreach ($lRate2Show as &$lExtrern) {
			$lDif = $lExtrern['tarief'] - $lRate;
			$lCalculatedWage = $selectedWage;
			if ($lDif > 0) {
				$lCalculatedWage =  round($selectedWage + ($lDif * 0.7) * 168);
			}	
			$lExtrern['wage'] = sprintf('%d', $lCalculatedWage);
		}
	}
	

	public function handle(){
		if (isset($_SESSION['xmlOptions'])) {
			$xmlstr = $_SESSION['xmlOptions'];
		} else {
			$xmlstr = file_get_contents(RESOURCE_PATH.DIRECTORY_SEPARATOR.'dpaOptions.xml');
			$_SESSION['xmlOptions'] = $xmlstr;
		}
		$this->xmlOptions = new SimpleXMLElement($xmlstr);		
		$this->updateSelectedOptions($this->mParams['request']);
		
	}


	public function show(TemplateParser $aParser){
		$aParser->setData('InternalRate',$this->mParams['request']['rate']);
		$aParser->setData('Gekozen',$this->mParams['Gekozen']);
		
		$aParser->setData('ExternTarief', $this->mParams['request']['rates2show']);
	}
}

