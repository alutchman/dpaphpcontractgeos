<?php 

class ContractStartModel extends  AbstractModel{
	
	private $mCvsData = array();
	private $mTariefData = array();
	
	private $lastGekozen = array();
	
	private function createCvsData(){
		$file = RESOURCE_PATH.DIRECTORY_SEPARATOR.'wage2rate.csv';
			
		$csv = array();
		$lines = file($file, FILE_IGNORE_NEW_LINES);
		foreach ($lines as  $value)
		{
			$csv[] = str_getcsv($value, ';');
		}
		
		array_walk($csv, function(&$a) use ($csv) {
			$a = array_combine($csv[0], $a);
		});
				
		array_shift($csv); # remove column header
	
		$this->mCvsData = array();
		foreach ($csv as $lKey=>$lValue) {
			$lSectedAttrib =  ''	;
			$lValue['selected'] = $lSectedAttrib;
			$lValue['wage'] = intval($lValue['wage']);
			$lValue['rate'] = intval($lValue['rate']);
			$this->mCvsData[intval($lValue['wage'])] = array('wage'=>intval($lValue['wage']), 'rate'=> intval($lValue['rate']) , 'selected'=>$lSectedAttrib);
		}
	}

	private function getFromSession($arg, $lDefault){
		if (array_key_exists($arg, $this->lastGekozen)) {
			return $this->lastGekozen[$arg];			
		}		
		return $lDefault;
	}
	
	public function handle(){
		if (isset($_SESSION['Wages2Rates'])) {
			$this->mCvsData = unserialize($_SESSION['Wages2Rates']);
		} else {
			$this->createCvsData();			
		}
		
		if (isset($_SESSION['gekozen'])) {
			$this->lastGekozen = unserialize($_SESSION['gekozen']);
		}

		
		$selectedWage = $this->getFromSession('wage', 2500);
		
		foreach ($this->mCvsData as $lKey=>$lValue) {
			$lSectedAttrib = intval($lValue['wage']) == $selectedWage ? 'selected="SELECTED"'  : ''	;
			$this->mCvsData[$lKey]['selected'] = $lSectedAttrib;
		}
		
		$_SESSION['Wages2Rates'] = serialize($this->mCvsData);				
		$selectedTarief = $this->getFromSession('extTarief', 60);

		$this->mTariefData = array();
		for ($tarief=60; $tarief<=120; $tarief +=5) {
			$lSectedAttrib = $tarief == $selectedTarief ? 'selected="SELECTED"'  : ''	;
			$this->mTariefData[] = array('tarief'=>$tarief, 'selected'=>$lSectedAttrib);
		}
	}


	public function show(TemplateParser $aParser){

		$aParser->setData('extTarief', $this->mTariefData);
		$aParser->setData('loonData', $this->mCvsData);

	}
}

