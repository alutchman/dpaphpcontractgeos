<?php


class DpaXmlModel extends  AbstractModel{

	private $xmlOptions;
	private $lSavedOptions = array();
	

	
	public function handle(){

		if (isset($_SESSION['xmlOptions']) ) {
			$xmlstr = $_SESSION['xmlOptions'];
		} else {
			$xmlstr = file_get_contents(RESOURCE_PATH.DIRECTORY_SEPARATOR.'dpaOptions.xml');
			$_SESSION['xmlOptions'] = $xmlstr;
		}

		
		$this->xmlOptions = new SimpleXMLElement($xmlstr);
		
		
		if (isset($_SESSION['gekozen'])) {
			$this->lSavedOptions = unserialize($_SESSION['gekozen']);
		} else {
			$this->lSavedOptions = array();
			$this->lSavedOptions['wage'] = '2500';
			foreach ($this->xmlOptions->option as $option) {
				$lFieldName = strval($option->attributes()->name);
				$this->lSavedOptions[$lFieldName] = '0';
			}			
		}
		$this->mParams['gekozen'] = &$this->lSavedOptions;
		
		
	}
	
	
	public function show(TemplateParser $aParser){
		$selections = array();
		foreach ($this->xmlOptions->option as $option) {
			$item = array();
			$item['name'] = strval($option->attributes()->name);
			$item['OptionsName'] = str_replace(' ', '_', $item['name'] );			
			
			
				
			$listItems = array();
			
			
			if (array_key_exists($item['OptionsName'], $this->lSavedOptions)) {
				$lSelected = $this->lSavedOptions[$item['OptionsName']];
			} else {
				$lSelected = '';
			}
			
			$item['label'] = strval($option->attributes()->title);
			
			foreach ($option->item as $listItem) {	
				$lExtra = strval($listItem->attributes()->value ) == $lSelected ? 'checked="checked"'  : '';
				$listItems[] = array(
						'value'=>strval($listItem->attributes()->value ), 
						'display'=>strval($listItem->attributes()->display),
						'mobile'=>strval($listItem->attributes()->mobile),
						'info'=>trim(strval($listItem)),
						'selected'=>$lExtra
				)  ;
			}			
			$aParser->setData($item['OptionsName'], $listItems);		
			$selections[] = $item;	   
		}
		
		$aParser->setData('xmloptions',$selections);
		

	}
}