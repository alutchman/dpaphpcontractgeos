<?php 

final class SpecialHandler {
	private function __construct(){
		throw new Exception('You are not allowed to instantiate this class');
	}
	
	public static function testInstantiate(){
		new SpecialHandler();
	}
	
	public static function init($libList){
		$lLib2use = array(get_include_path());
		foreach ($libList as $lLib) {
			$lLib2use[] = PROJECTPATH.DIRECTORY_SEPARATOR.$lLib;
		} 
		set_include_path(implode(PATH_SEPARATOR,$lLib2use));
	
		spl_autoload_register(sprintf('%s::autoloader',get_called_class()));
		set_exception_handler(sprintf('%s::handleException',get_called_class()));
	}
	
	public static function handleException(Exception $e){
		$lclass = get_called_class();
		$lData = array(
			'message'=>$e->getMessage(),
			'file'=>$e->getFile(),
			'line'=>$e->getLine(),
		);
		
		if ($lclass !== false) {
			$lListTrace =  $e->getTrace();
			$lLastIndex = count($lListTrace)-1;
			if ($lLastIndex >= 0) {
				$lData['line']= $lListTrace[$lLastIndex]['line'];
				$lData['file']= $lListTrace[$lLastIndex]['file'];
			}
		}
		//error_log($e->getMessage());
		echo('<pre>');
		print_r($lData);
		echo('</pre>');
	}
	
	public static function autoloader($class_name){
		$lSearchFile = sprintf('%s.php',$class_name);
		$lPaths = explode(PATH_SEPARATOR,get_include_path() );
		foreach($lPaths as $lPath){
			$lSearchFullFile = 
				sprintf('%s%s%s.php',$lPath,DIRECTORY_SEPARATOR,lcfirst($class_name));
			if (file_exists($lSearchFullFile)){
				require_once $lSearchFullFile;
				if (class_exists($class_name)) return;
			}
		}

		
		
		if (strpos($class_name,'Controller') > 0) {
			throw new AutoloadException("Unable to load $class_name class.");
		} 
		
		throw new AutoloadException("Unable to load $class_name class.");
	}
}