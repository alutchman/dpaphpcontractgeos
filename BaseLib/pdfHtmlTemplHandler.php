<?php

class PdfHtmlTemplHandler extends TemplateParser{


	
	public function show(){
		ini_set('max_execution_time',300);
	
		if ($this->layout !== null && !file_exists($this->layout)) {
			throw new TemplateException(sprintf('The requested layout <font style="color:red;">%s</font> is '.
					'not found on your file system .',
					$this->layout));
		}
	
		if (!file_exists($this->currentTemplateDir.$this->mFile)) {
			throw new TemplateException(sprintf('The requested template <font style="color:red;">%s</font> is '.
					'not found on your file system in the directory <font style="color:red;">%s</font>.',
					$this->mFile, $this->mTemplatePath.DIRECTORY_SEPARATOR));
		}
		$aFile = $this->currentTemplateDir.$this->mFile;
		
		if ($this->layout !== null && file_exists($this->layout)) {
			$aFile = $this->layout;
		}
		
		
		if (!file_exists($aFile)){
			throw new TemplateException('Included template not found:'.basename($aFile). '[Path='.dirname($aFile).']');
		}
		
		$lDataCollected = $this->readFileAndUpdateGenerals($aFile);
		$this->handleContent($lDataCollected);
		$this->updateDataAdded($lDataCollected);
		
	
		return $lDataCollected;
	}
	
	private function updateDataAdded(&$aTemplateContents){		
		$this->handleBufferedIncludes($aTemplateContents);
		$this->handleLists($aTemplateContents);
		//	self::replaceAnchors($lReadHandle, $aTemplateData);
		
	}
	
	
	private function handleContent(&$aTemplateContents){
		$lRegs = null;
		if (preg_match_all('|<content/>|', $aTemplateContents, $lRegs)) {
			$lLoadedTemplate = new PdfHtmlTemplHandler($this->mTemplatePath, $this->mFile);		
			$CurTemplateDir  = TEMPLATE_BASE . DIRECTORY_SEPARATOR . $this->mTemplatePath.DIRECTORY_SEPARATOR;
			$lReplacedData = $this->readFileAndUpdateGenerals($CurTemplateDir.$this->mFile);
			
		
			$aTemplateContents = str_replace('<content/>', $lReplacedData, $aTemplateContents);
		}
	}
	
	
	/**
	 * The next function will look for tags like <include>.....</include> and
	 * use the text between the tags to get the file that needs to be included.
	 */
	private function handleBufferedIncludes(&$aTemplateContents){
		$lRegs = null;
		if (preg_match_all('/<include>(?<includeData>.*)<\/include>/Us', $aTemplateContents, $lRegs)) {
			$lListData  = $lRegs['includeData'];
			for ($i=0; $i<count($lListData); $i++) {
				$lIncludeInfo  = $lListData[$i];
	
				$lPathToCheck = trim($lIncludeInfo);
				$lFile = ($lPathToCheck);
				$lPathToCheck = str_replace('/', DIRECTORY_SEPARATOR, $lPathToCheck);
				$lPathToCheck = str_replace('\\', DIRECTORY_SEPARATOR, $lPathToCheck);	
				$lTemplatePath = (strpos($lPathToCheck, '~'.DIRECTORY_SEPARATOR) === 0) ? dirname(substr($lPathToCheck, 2)) : $this->mTemplatePath;
				$CurTemplateDir  = TEMPLATE_BASE . DIRECTORY_SEPARATOR . $lTemplatePath.DIRECTORY_SEPARATOR;

				$lReplacedData = $this->readFileAndUpdateGenerals($CurTemplateDir.$lFile);		
				$aTemplateContents = str_replace($lRegs[0][$i], $lReplacedData,$aTemplateContents);
			}
		}
	}
	
	
	private function readFileAndUpdateGenerals($lFile2Read){
		$lReplacedData = file_get_contents($lFile2Read);
		self::replaceGeneralValues($lReplacedData);
		$this->updateDataAdded($lReplacedData);		
		return $lReplacedData;
	}

	
	private function handleLists(&$aTemplateContents) {
		$lRegs = null;
		while (preg_match_all('!<list(?<listName>.*)>(?<listData>.*)<\/list\1>!Us', $aTemplateContents, $lRegs)) {
			$lListData  = $lRegs['listData'];
			$lListNames = $lRegs['listName'];
			for ($i=0; $i< count($lListNames); $i++) {
				$lListName        = trim($lListNames[$i]);
				$lListDataItem    = $lListData[$i];
				
				$this->updateDataAdded($lListDataItem);
					
				$lReplacedData = $this->iterateListItems($lListName,  $lListDataItem);		
				$aTemplateContents = str_replace($lRegs[0][$i], $lReplacedData, $aTemplateContents);
			}
		}
	}
	
	
	
	
	private function iterateListItems($lListName,  $lData2Use) {
		$lReplacedData = '';
		
		if (array_key_exists($lListName, self::$mListData )) {
		
			for ($j=0; $j< count( self::$mListData[$lListName]) ; $j++) {
				$lListItemData = $lData2Use;
				$lValueIndex = self::$mListData[$lListName][$j];
				if (is_array($lValueIndex) ) {
					foreach($lValueIndex as $lKey=>$lValue) {
						if (!is_array($lValue)) {
							$lListItemData = str_replace('{#'.$lKey.'#}', $lValue, $lListItemData);
						} else {		
		
							//TODO: what if we get an array here....not supported yet...
						}
					}
				}
				$lReplacedData .= $lListItemData;
			}
		}	
		//=== clean up===
		if (preg_match_all('/{#(\w+)#}/U', $lReplacedData, $lRegs)) {
			$vars = $lRegs[1];
			reset($vars);
			while(list($key, $value) = each($vars)) {
				$lReplacedData = str_replace('{#'.$value.'#}', '&nbsp;', $lReplacedData);
			}
		}			
	    return 	$lReplacedData;
	}
}

