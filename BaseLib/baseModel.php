<?php 
interface BaseModel {
	public function handle();
	public function show(TemplateParser $aParser);
	public function __construct(&$mParams);
}
