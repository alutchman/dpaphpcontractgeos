<?php

final class RouteParamsHandler
{

	private function __construct(){
		//avoid instantiating this function
	}


	public static function matchFunction($aMethodName, $aRegs)
	{
		$lCalledClass = get_called_class();
		$lMatched = false;
		if (method_exists($lCalledClass, $aMethodName ) ) {
			return self::$aMethodName($aRegs);
		} else {
			error_log(sprintf('%s::%s Method does NOT exist!',$lCalledClass ,$aMethodName));
		}
		return $lMatched;
	}


	private static function regex_Href($aRegs){
		return array(
				'data'=> urldecode($aRegs['data']), 
				'controller'=>'data'
		);
	}


	private static function regex_CtrlAction($aRegs)
	{
		$lRequest               = array();
		$lRequest['controller'] = $aRegs['controller'];
		$lRequest['action']     = $aRegs['action'];
		$lRequest['ajax']     = false;
		return $lRequest;
	}


	private static function regex_AjaxCtrlAction($aRegs)
	{
		$lRequest               = array();
		if (array_key_exists('controller', $aRegs)){
			$lRequest['controller'] = $aRegs['controller'];
		}
		$lRequest['action']     =  $aRegs['action'];
		$lRequest['ajax']       = true;
		return $lRequest;
	}



	private static function regex_CtrlIndex($aRegs)
	{
		$lRequest               = array();
		$lRequest['controller'] = $aRegs['controller'];
		$lRequest['action']     = 'index';
		return $lRequest;
	}

	private static function regex_AjaxCtrlIndex($aRegs)
	{
		$lRequest               = array();
		$lRequest['controller'] = $aRegs['controller'];
		$lRequest['action']     = 'index';
		$lRequest['ajax']       = true;
		return $lRequest;
	}



	private static function regex_graphData($aRegs)
	{
		$lController = $aRegs['controller'];
		$lAction     = $aRegs['action'];
		$lData       = $aRegs['data'];
		$lRequest               = array();
		$lRequest['controller'] = $lController;
		$lRequest['action']     = $lAction;
		$lRequest['data']       = $lData;
		$lRequest['ajax']       = false;
		$lRequest['ImageType']  = 'ON';
		$lRequest['imageData']  = 'ON';
		return $lRequest;
	}

	
	private static function regex_MainAction($aRegs)
	{
		$lRequest               = array();
		$lRequest['controller'] = 'Main';
		$lRequest['action']     = $aRegs['action'];
		return $lRequest;
	}
	
}