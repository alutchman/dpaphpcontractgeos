<?php

class Administrator {
	private $mParams     = array();
	private $mAction     = null;
	private $mAllowed    = true;
	private $mControllerClass = null;

	public function __construct() {

	}

	public function setParams(Array $aParams) {
		$this->mParams = $aParams;
		if (!array_key_exists('ajax', $this->mParams))   $this->mParams['ajax'] = false;
		if (!array_key_exists('action', $this->mParams)) $this->mParams['action'] = 'index';
		$this->mAction           = $this->mParams['action'];
		$this->mControllerClass  = sprintf('%sController', ucfirst($this->mParams['controller']));
		unset($this->mParams['controller']);
	}
	
	
	public function chooseHandeler($controller, $action = null) {
		$DataHandler = array();
		
		$DataHandler['controller'] = $controller;
		if (!is_null($action)) {
			$DataHandler['action'] = $action;
		}
		
		$this->setParams($DataHandler);
		
		
	}

	public function getAction(){
		return $this->mAction;
	}

	public function getControllerClass(){
		return $this->mControllerClass;
	}


	protected function evaluateAccess(){
		//$this->mAllowed = false;
	}


	public function handleRequest(){
		$this->evaluateAccess();
		if ($this->mAllowed) {
			$reflectionClass = new ReflectionClass($this->mControllerClass);
			if (!$reflectionClass->IsInstantiable()) {
				throw new AbstractException(sprintf('%s class is abstract and cannot be instantiated !!',$this->mControllerClass));
			}
			$lController = new  $this->mControllerClass($this, $_REQUEST);
			$lController->executeAction();
		} else {
			$lAuthError = new AuthException('You are not authorized to view this page.');
			$lAuthError->setController($this->getControllerClass());
			$lAuthError->setAction( $this->mParams['action']);
			throw $lAuthError;
		}
	}


	public function redirect($altPath){
		header(sprintf('Location: %s%s',BASEPATH,$altPath));
	}
	
	
	public function handleException(Exception $e, $lAltAction = 'general' ){
		$lController = new  ErrorController($this, $_REQUEST, $e);
		$lController->executeAltAction($lAltAction);
	}
}