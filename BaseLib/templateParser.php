<?php 

class TemplateParser {
	protected $mBasePath = '';
	protected $mTemplatePath = '';
	protected $mFile= '';
	
	protected static $mInitialized = false;

	protected static  $mPrimitiveData = array();
	protected static  $mListData      = array();
	protected static  $mBooleanData   = array();
	protected $mViewData;
	
	protected $isInclude = false;

	protected $currentTemplateDir;
	
	protected $layout = null;
	protected $buffering = true;
	protected $delay = 0; // 0, 100000


	public static function initialize(){
		if (self::$mInitialized == true) return;
		self::$mListData['POST']    =$_POST;
		self::$mListData['GET']     =$_GET;
		self::$mListData['COOKIES'] =$_COOKIE;
		self::$mListData['REQUEST'] =$_REQUEST;
		
		$constants = get_defined_constants(true);
		$userConstants = $constants['user'];
		$lUserInfo = array();
		foreach ($userConstants as $key=> $lvalue) {
			$lUserInfo[$key] = $lvalue;
		}
		self::$mListData['CONST']    = $lUserInfo;
		
		self::$mInitialized = true;
	}
	
	
	public function setTypeIncluded(){
		$this->isInclude = true;
	}
	
	public function disableBuffering(){
		$this->buffering = false;
	}


	public function __construct($aTamplatePath, $aFile, $aControllerParams = null){
		$this->mBasePath = TEMPLATE_BASE;				
		$this->mTemplatePath = $aTamplatePath;
		$this->mFile = $aFile;
		
		$this->currentTemplateDir  = $this->mBasePath . DIRECTORY_SEPARATOR . $aTamplatePath.DIRECTORY_SEPARATOR;
		
		self::initialize();
		if (!is_null($aControllerParams)){
			$this->updateParams($aControllerParams);
		}
		
		$this->setData('basePath',BASEPATH);
		if (!is_null(constant('COPYRIGHT'))){
			$this->setData('copyright_text',constant('COPYRIGHT'));
		}
	}
	
	public function setLayout($aLayout){
		$this->layout = $aLayout;
	}


	public function updateParams($aControllerParams){
		foreach($aControllerParams as $lKey=>$lValue){
			if (strlen($lKey) > 1){
				$this->setData($lKey, $lValue);
			}
		}
	}


	public function setData($lKey, $lValue) {
		if (is_bool($lValue)) {
			self::$mBooleanData[$lKey] = $lValue;
		} elseif (is_array($lValue)) {
			self::$mListData[$lKey] = $lValue;
		} else {
			self::fix_UTF8_Encoding($lValue);
			self::$mPrimitiveData[$lKey] = $lValue;
		}
	}


	public function show(){
		ini_set('max_execution_time',300);
		
		if ($this->layout !== null && !file_exists($this->layout)) {
			throw new TemplateException(sprintf('The requested layout <font style="color:red;">%s</font> is '.
				'not found on your file system .',
				$this->layout));
		}
		
		if (!file_exists($this->currentTemplateDir.$this->mFile)) {
			throw new TemplateException(sprintf('The requested template <font style="color:red;">%s</font> is '.
					'not found on your file system in the directory <font style="color:red;">%s</font>.',
					$this->mFile, $this->mTemplatePath.DIRECTORY_SEPARATOR));
		}
		
		if ($this->buffering) {
			ini_set('output_buffering','On') ;
			ob_start();
			header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			header("Connection: close");
			ob_flush();
			ob_end_flush();			
		}
		

		$this->parseTemplateText();
		
		if ($this->buffering) {
			ob_end_flush();
		}
		
	}


	private function parseTemplateText(){
		$aFile = $this->currentTemplateDir.$this->mFile;
		
		if ($this->layout !== null && file_exists($this->layout)) {
			$aFile = $this->layout;
		}
		
		
		if (!file_exists($aFile)){
			throw new TemplateException('Included template not found:'.basename($aFile). '[Path='.dirname($aFile).']');
		}
		
		$lDataCollected = "";
		
		$lReadHandle = @fopen($aFile, 'r');
		if ($lReadHandle !== false){
			while($line = fgets($lReadHandle)) {
				$lPos = ftell($lReadHandle);
				$lBuffer = $this->handleLine($lReadHandle, $line);
				
				if ($this->isInclude) {	
					self::replaceGeneralValues($lBuffer);
					$lDataCollected .= $lBuffer;
				} else {
					$this->printNextBuffer($lBuffer);
				}				
			}
			fclose($lReadHandle);
		}
		return $lDataCollected;
	}


	private function handleLine(&$lReadHandle, $lBuffer){	
		self::replaceGeneralValues($aTemplateData);
		$this->handleContent($lBuffer);
		self::replaceGeneralValues($aTemplateData);
		$this->handleBufferedIncludes($lBuffer);		
		$this->handleLists($lReadHandle, $lBuffer);
		self::replaceGeneralValues($lBuffer);		
		return $lBuffer;		
	}
	
	private function handleContent(&$aTemplateContents){
		$lRegs = null;
		if (preg_match_all('|<content/>|', $aTemplateContents, $lRegs)) {			
			$lLoadedTemplate = new TemplateParser($this->mTemplatePath, $this->mFile);
			$lLoadedTemplate->parseTemplateText();
			$aTemplateContents = str_replace('<content/>', '', $aTemplateContents);
		}
	}


	private function printNextBuffer($aBuffer) {		
	//	$this->handleBufferedIncludes($aBuffer);	
	//	self::replaceGeneralValues($aBuffer);

		if ($this->buffering) {
			print($aBuffer);
			ob_flush();
			flush();
			usleep($this->delay);
		} else {
			echo $aBuffer;
		}
	}
	
	
	

	/**
	 * Here we expand the list with
	 *
	 * @param unknown $lReadHandle
	 * @param unknown $aTemplateContents
	 * @return boolean
	 */
	private function initInclude(&$lReadHandle, &$aTemplateContents){
		$innerListData = "";
		$lRegs = null;
		if (preg_match_all('/<list(?<listName>[\w]+)>/U', $aTemplateContents, $lRegs)) {
			$lListNames = $lRegs['listName'];
			for ($i=0; $i< count($lListNames); $i++) {
				$lListName        = $lListNames[$i];
				$lLookFor = sprintf('</list%s>', $lListName);
				while (strpos($aTemplateContents, $lLookFor) === false) {
					if (feof($lReadHandle))  break;
					$innerListData .= fgets($lReadHandle);
	
				}
				if (feof($lReadHandle))  break;
			}
			return $innerListData;
		}
		return false;
	}

	
	/**
	 * The next function will look for tags like <include>.....</include> and
	 * use the text between the tags to get the file that needs to be included.
	 */
	private function handleBufferedIncludes(&$aTemplateContents){
		$lRegs = null;
		if (preg_match_all('/<include>(?<includeData>.*)<\/include>/Us', $aTemplateContents, $lRegs)) {
			$lListData  = $lRegs['includeData'];
			for ($i=0; $i<count($lListData); $i++) {
				$lIncludeInfo  = $lListData[$i];				
				
				$lPathToCheck = trim($lIncludeInfo);				
				$lFile = basename($lPathToCheck);				
				$lPathToCheck = str_replace('/', DIRECTORY_SEPARATOR, $lPathToCheck);
				$lPathToCheck = str_replace('\\', DIRECTORY_SEPARATOR, $lPathToCheck);
				
				
				$lTemplatePath = (strpos($lPathToCheck, '~'.DIRECTORY_SEPARATOR) === 0) ? dirname(substr($lPathToCheck, 2)) : $this->mTemplatePath;
							
				$lLoadedTemplate = new TemplateParser($lTemplatePath, $lFile);
				$lLoadedTemplate->setTypeIncluded();				
				$lReplacedData = $lLoadedTemplate->parseTemplateText();

				//===Replaced data may contain more includes	
				$lLoadedTemplate->handleBufferedIncludes($lReplacedData);				
				$aTemplateContents = str_replace($lRegs[0][$i], $lReplacedData,$aTemplateContents);
			}
			//=== We may have new includes in the included files TemplateParser
		}
	}



	public static function fix_UTF8_Encoding(&$in_str) {
		$cur_encoding = mb_detect_encoding($in_str, "auto");
		if($cur_encoding == 'UTF-8' && !mb_check_encoding($in_str,'UTF-8')) {
			$in_str =   utf8_encode($in_str);
		}
	}

	private static function replaceTypicalVars($aType, &$aTemplateData)
	{
		$lRegex = sprintf('!{{%s\[\'(?<itemKey>[\w\_\s]+)\'\]}}!U',$aType);
		while (preg_match_all($lRegex, $aTemplateData, $lRegs)) {
			$lListKeys = $lRegs['itemKey'];
			for ($i=0; $i< count($lListKeys); $i++) {
				$lKey = $lListKeys[$i];
				$lReplacedData = '';
				if(isset(self::$mListData[$aType][$lKey])) {
					$lReplacedData = self::$mListData[$aType][$lKey];
				}
				$lDataToReplaceSQ =  sprintf('{{%s[\'%s\']}}', $aType,  $lKey);
				$lDataToReplaceDQ =  sprintf('{{%s["%s"]}}'  , $aType,  $lKey);
				$aTemplateData = str_replace($lDataToReplaceSQ,$lReplacedData , $aTemplateData);
				$aTemplateData = str_replace($lDataToReplaceDQ,$lReplacedData , $aTemplateData);
			}
		}
	}


	private static function replaceAnchors(&$lReadHandle , &$aTemplateData){
		//do not forget Us.. .for ungreedy and allowing white space characters like carriage return and line feed.
		if (preg_match_all('!\<a(?<preStuff>.*)href="(?<imageURL>.*)"(?<postStuff>.*)\>!U', $aTemplateData, $lRegs)) {
			for ($i=0; $i< count($lRegs['imageURL']); $i++) {
				if (strpos($lRegs['imageURL'][$i],BASEPATH) === false) {
					$lReplacedData        = sprintf('<a%shref="%s%s"%s>', $lRegs['preStuff'][$i], BASEPATH,$lRegs['imageURL'][$i],$lRegs['postStuff'][$i]);
					$lDataToReplaceSQ =   $lRegs[0][$i];
					$aTemplateData = str_replace($lDataToReplaceSQ,$lReplacedData , $aTemplateData);
				}
			}
		} elseif (preg_match_all('!\<a(?<postStuff>.*)!U', $aTemplateData, $lRegs)) {
			if (feof($lReadHandle))  return;
			$aTemplateData .= fgets($lReadHandle);
			self::replaceAnchors($lReadHandle , $aTemplateData);
		}
	}


	private static function replaceAttrib(&$aTemplateData,$lPrefix,$lStartsWith='/'){

		if (preg_match_all(sprintf('!%s="%s(?<imageURL>.*)"!U',$lPrefix,$lStartsWith), $aTemplateData, $lRegs)) {
			$lImageUrls = $lRegs['imageURL'];
			for ($i=0; $i< count($lImageUrls); $i++) {
				$lReplacedData        = sprintf('%s="%s%s%s"',$lPrefix,BASEPATH,$lStartsWith,$lImageUrls[$i]);
				$lDataToReplaceSQ = $lRegs[0][$i];
				$aTemplateData = str_replace($lDataToReplaceSQ,$lReplacedData , $aTemplateData);
			}
		}
		if (preg_match_all(sprintf('!%s=\'%s(?<imageURL>.*)\'!U',$lPrefix,$lStartsWith), $aTemplateData, $lRegs)) {
			$lImageUrls = $lRegs['imageURL'];
			for ($i=0; $i< count($lImageUrls); $i++) {
				$lReplacedData        = sprintf('%s=\'%s%s%s\'',$lPrefix,BASEPATH,$lStartsWith,$lImageUrls[$i]);
				$lDataToReplaceSQ = $lRegs[0][$i];
				$aTemplateData = str_replace($lDataToReplaceSQ,$lReplacedData , $aTemplateData);
			}
		}
	}

	protected static function replaceGeneralValues( &$aTemplateData){
		self::replaceTypicalVars('POST'   , $aTemplateData);
		self::replaceTypicalVars('GET'    , $aTemplateData);
		self::replaceTypicalVars('COOKIES', $aTemplateData);
		self::replaceTypicalVars('REQUEST', $aTemplateData);	
		self::replaceTypicalVars('CONST', $aTemplateData);
		
		while (preg_match_all('/{{(\w+)}}/U', $aTemplateData, $lRegs)) {
			$vars = $lRegs[1];
			reset($vars);
			$i=0;
			while(list($key, $value) = each($vars)) {
				$aTemplateData = str_replace($lRegs[0][$i], (isset(self::$mPrimitiveData[$value])) ? self::$mPrimitiveData[$value] : '', $aTemplateData);
			}
		}
	}

	private function handleLists(&$lReadHandle, &$aTemplateContents) {
		$lRegs = null;
		if (preg_match_all('!<list(?<listName>.*)>!U', $aTemplateContents, $lRegs)) {
			$lListNameOuter        = $lRegs['listName'][0];

			$lLookFor = sprintf('</list%s>', $lListNameOuter);
			while (strpos($aTemplateContents, $lLookFor) === false) {
				if (feof($lReadHandle))  break;
				$aTemplateContents .= fgets($lReadHandle);		
			}

			while (preg_match_all('!<list(?<listName>.*)>(?<listData>.*)<\/list\1>!Us', $aTemplateContents, $lRegs)) {
				$lListData  = $lRegs['listData'];
				$lListNames = $lRegs['listName'];
				for ($i=0; $i< count($lListNames); $i++) {
					$lListName        = trim($lListNames[$i]);
					$lListDataItem    = $lListData[$i];
									
					$lReplacedData = $this->updateListItemData($lListName, $lListDataItem);
					$aTemplateContents = str_replace($lRegs[0][$i], $lReplacedData, $aTemplateContents);
				}
			}			
		}
	}
	
	
	private function updateListItemData($lListName, $lListDataItem){
		$lReplacedData = $lListDataItem;		
		
		if (array_key_exists($lListName, self::$mListData )) {
			$lResultData = '';
			foreach (self::$mListData[$lListName] as $lValueIndex) {
				$lItemData = $lListDataItem;		
				if (is_array($lValueIndex) ) {
					foreach($lValueIndex as $lKey=>$lValue) {
						if (!is_array($lValue)) {
							$lItemData = str_replace('{#'.$lKey.'#}', $lValue, $lItemData);
						}
					}
				}
				$lResultData .= rtrim($lItemData);
			}
			$lReplacedData = ltrim($lResultData);
		}	
		return $lReplacedData;
	}
}

