<?php
session_start();
require_once 'config.ini.php';
require_once('BaseLib/baseModel.php');
require_once('BaseLib/specialHandler.php');

SpecialHandler::init(array('BaseLib','Controllers','Exceptions','Models', 'libs' ));

try{
	$localAdmin = new Administrator();
	$localAdmin->chooseHandeler('main', 'ajaxResetSession');
	$localAdmin->handleRequest();
} catch(AuthException $e) {
	$localAdmin->handleException($e,'noAuth');
} catch(Exception $e) {
	$localAdmin->handleException($e);
}
