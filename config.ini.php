<?php

date_default_timezone_set('Europe/Amsterdam');
define('BASEPATH'              , '/');
define('PROJECTPATH'           ,__DIR__);
define('RESOURCE_PATH'         , PROJECTPATH.DIRECTORY_SEPARATOR.'resources');
define('TEMPLATE_BASE'         , PROJECTPATH.DIRECTORY_SEPARATOR.'Templates');
define('LAYOUT'                , TEMPLATE_BASE.'/includes/geos.layout.html'); 
define('LAYOUT_MOBILE'         , TEMPLATE_BASE.'/includes/layout.mobile.html');
define('DOWNLOAD_HTML'         , TEMPLATE_BASE.'/includes/downloadLayout.html');
define('CONTRACT_LAYOUT'       , TEMPLATE_BASE.'/includes/contractLayout.html');
define('PDF_PAD_INCLUDE'       , PROJECTPATH.DIRECTORY_SEPARATOR.'libs'.DIRECTORY_SEPARATOR.'tcpdf');
define('PDF_HTML_INCLUDE'      , '/var/www/html2pdf');
define('COPYRIGHT'             , sprintf('&copy; Copyrights %d DPA Group N.V &reg;. All rights reserved',date('Y')));
define('ADMIN_MAIL'            , 'amrit.lutchman@dpa.nl');
define('ADMIN_MAIL_ACCESS'     , 'ujyFa1de');

define('DPA_RESOURCE_NAME'     , 'Amrit Lutchman');
define('DPA_RESOURCE_EMAIL'    , 'amrit.lutchman@dpa.nl');
define('DPA_DOCS_DIR'          , PROJECTPATH.DIRECTORY_SEPARATOR.'docs');
define('DPA_LOGO'              , PROJECTPATH.DIRECTORY_SEPARATOR.'images/DPA-GEOS.jpg');
define('DPA_PLAATS'		       , 'Amsterdam');
define('DPA_MANAGER'           , 'De heer drs. O.O. Berten');
